package com.smartpesa.visakernel.apptest.nfc;

public interface CardTabListener {

    void cardTaped();

    void transmissionError();

    void timeOutError();

    void protocolError();

}
