package com.smartpesa.visakernel.apptest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.smartpesa.visakernel.VisaKernelApplication;
import com.smartpesa.visakernel.apptest.nfc.CardTabListener;
import com.smartpesa.visakernel.utility.Utils;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VisaKernelApplication.initVisaKernel(MainActivity.this);

        // Example of a call to a native method
        final TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText("please tap card");

        new AsyncTask<Void, byte[], byte[]>() {
            @Override
            protected byte[] doInBackground(Void... voids) {
                HALHandler halHandler = new HALHandler();
                byte[] response = halHandler.sendAPDUCommand("00A404000E325041592E5359532E444446303100");
                return response;
            }

            @Override
            protected void onPostExecute(byte[] response) {
                super.onPostExecute(response);
                tv.setText(Utils.bytesToHexString(response));
            }
        }.execute();

    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
