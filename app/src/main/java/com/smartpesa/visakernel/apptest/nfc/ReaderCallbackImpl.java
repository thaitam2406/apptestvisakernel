package com.smartpesa.visakernel.apptest.nfc;

import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Build.VERSION_CODES;
import android.support.annotation.RequiresApi;
import android.util.Log;

@RequiresApi(api = VERSION_CODES.KITKAT)
class ReaderCallbackImpl implements NfcAdapter.ReaderCallback {

    private TagEventListener mTagEventListener;

    ReaderCallbackImpl(final TagEventListener tagEventListener) {
        this.mTagEventListener = tagEventListener;
    }

    @Override
    public void onTagDiscovered(final Tag onTagDiscovered) {
        Log.d("onTagDiscovered",onTagDiscovered.toString());
        mTagEventListener.setIsoDep(IsoDep.get(onTagDiscovered));
    }

}
