package com.smartpesa.visakernel.apptest;

import android.app.Activity;
import android.util.Log;

import com.smartpesa.visakernel.VisaKernelApplication;
import com.smartpesa.visakernel.apptest.nfc.NFCProvider;
import com.smartpesa.visakernel.exception.L1RSPException;
import com.smartpesa.visakernel.hal.AbstractHALInteract;
import com.smartpesa.visakernel.listeners.CardCommunicationProvider;
import com.smartpesa.visakernel.utility.Utils;

public class HALHandler implements AbstractHALInteract {

    final String TAG = HALHandler.class.getSimpleName();

    /**
     * Get terminal property, like TTQ, TVR, Floor Limit
     */
    @Override
    public byte[] terminalProperties(String tlv) {
        return new byte[0];
    }

    @Override
    public byte[] sendAPDUCommand(String command) {
        byte[] mPpseResponse;
        Activity activity = VisaKernelApplication.getActivity();
        CardCommunicationProvider mCardComms = new NFCProvider(activity);

        mCardComms.disconnectReader();

        try {
            mCardComms.connectReader();
            mCardComms.connectCard();
            Log.d(TAG, "Sending PPSE command");

            String ppseCommand = command;//"00A404000E325041592E5359532E444446303100"; // retrieve a list of available applications (and their AIDs)
            byte[] commandBytes = Utils.hexStringToBytes(ppseCommand);
            Log.d("CMD>> %s", ppseCommand);
            mPpseResponse = mCardComms.sendReceive(commandBytes);
            Log.d("RSP<< %s" , Utils.bytesToHexString(mPpseResponse));
            if (isSuccess(mPpseResponse)) {
                if (mCardComms != null && mCardComms instanceof NFCProvider) {
                    ((NFCProvider)mCardComms).cardTabFinish();
                }
                return mPpseResponse;
            }else{
                Log.d(TAG, "send APDU command failed");
            }

        } catch (L1RSPException e) {
            e.printStackTrace();
        }



        return new byte[0];
    }

    private boolean isSuccess(byte[] responseBytes) {
        // copy status word
        if (responseBytes.length > 2) {
            byte[] statusWord = new byte[]{responseBytes[responseBytes.length - 2],
                    responseBytes[responseBytes.length - 1]};
            if ((statusWord[0] & 0x00FF) == 0x0090 && statusWord[1] == 0x00)
                return true;
        }
        return false;
    }
}
