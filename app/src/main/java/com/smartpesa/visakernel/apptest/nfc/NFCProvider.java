package com.smartpesa.visakernel.apptest.nfc;

import android.app.Activity;
import android.nfc.TagLostException;
import android.nfc.tech.IsoDep;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.RequiresApi;
import android.util.Log;


import com.smartpesa.visakernel.exception.L1RSPException;
import com.smartpesa.visakernel.listeners.CardCommunicationProvider;

import java.io.IOException;

public class NFCProvider implements CardCommunicationProvider {

    final String TAG = NFCProvider.class.getSimpleName();

    private final NFCManager mNFCManager;
    private IsoDep mIsoDep;
    private TagEventListener mTagEventListener;
    private boolean isCardTapped;
    private CardTabListener cardTapListener;

    /**
     * Command execution time in nano seconds
     */
    private long mCommandExecutionTime = 0;

    public NFCProvider(Activity currentContext) {

        mNFCManager = new NFCManager(currentContext);
        // Check if NFC is enabled
        if (!mNFCManager.isNFCEnabled()) {
            Log.d(TAG, "NFC is not enabled");

        }
        disconnectReader();
    }

    @Override
    public byte[] sendReceive(byte[] bytes) throws L1RSPException {

        byte[] response = new byte[0];
        try {
            if (mIsoDep.isConnected()) {
                long startTime = System.nanoTime();
                response = mIsoDep.transceive(bytes);
                long endTime = System.nanoTime();
                // Command execution time in nano seconds
                mCommandExecutionTime = endTime - startTime;
            } else {
                Log.d(TAG, "sendReceive: ISO DEP obtained as null");
                cardTapFail();
                isCardTapped = false;
            }
        } catch (TagLostException exception) {
            Log.d(TAG, "TagLostException", exception.getCause());
            isCardTapped = false;
            if(cardTapListener != null) {
                cardTapListener.timeOutError();
                resetListener();
            }

        } catch (IOException e) {
            isCardTapped = false;
            if(cardTapListener != null) {
                cardTapListener.transmissionError();
                resetListener();
            }

        }
        if (response.length < 2) {
            if(cardTapListener != null) {
                cardTapListener.transmissionError();
                resetListener();
            }
        }

        return response;
    }

    @Override
    public boolean connectCard() throws L1RSPException {

        while (!isCardTapped) {
            try {
                mIsoDep = mTagEventListener.getIsoDep();
            } catch (Exception e) {
                Log.d(TAG, "An Exception was encountered: ", e.getCause());
            }
            if (mIsoDep != null) {
                try {
                    mIsoDep.connect();
                    mIsoDep.setTimeout(600);
                    isCardTapped = true;
                    return true;
                } catch (IOException | IllegalStateException e) {
                    if(cardTapListener != null) {
                        cardTapListener.protocolError();
                        resetListener();
                    }
                }
            }else{
//                Log.d(TAG, "mIsoDep null ");
            }
        }
        return isCardTapped;
    }

    @Override
    public boolean removeCard() {

        if (mIsoDep != null && mIsoDep.isConnected()) {
            try {
                mIsoDep.close();
                isCardTapped = false;
                mIsoDep = null;
                return true;
            } catch (IOException e) {
                return false;
            }
        } else {
            Log.d(TAG, "removeCard: IsoDep is null or disconnected");
            return true;
        }
    }

    @RequiresApi(api = VERSION_CODES.KITKAT)
    @Override
    public boolean connectReader() throws L1RSPException {

        if (mIsoDep == null) {
            // Establish Connection with reader
            mTagEventListener = new TagEventListener();
            mNFCManager.enableNFCReaderMode(new ReaderCallbackImpl(mTagEventListener));
            mIsoDep = null;
            isCardTapped = false;
        }
        return true;
    }

    @Override
    public boolean disconnectReader() {
        try {
            mIsoDep.close();
        } catch (IOException e) {
            return false;
        } catch (NullPointerException npe) {
            return true;
        }

        if (VERSION.SDK_INT >= VERSION_CODES.KITKAT) {
            mNFCManager.disableNFCReaderMode();
        }

        mIsoDep = null;
        isCardTapped = false;
        return true;
    }

    @Override
    public boolean isReaderConnected() {
        return true;
    }

    @Override
    public boolean isCardPresent() {
        return mIsoDep.isConnected();
    }


    @Override
    public String getDescription() {
        return "Built-in NFC Controller";
    }

    @Override
    public InterfaceType getInterfaceType() {
        return InterfaceType.CONTACTLESS;
    }

    @Override
    public int getPreviousCommandExecutionTime() {
        // return command execution time in microseconds
        return (int) mCommandExecutionTime / 1000;
    }

    public void setCardTapListener(CardTabListener cardTapListener) {
        this.cardTapListener = cardTapListener;
    }

    public void cardTabFinish(){
        Log.d(TAG, "cardTapFinish with delay");
        if(cardTapListener != null) {
            cardTapListener.cardTaped();
            resetListener();

        }
    }

    public void cardTapFail(){
        Log.d(TAG, "cardTapFailed");
        if(cardTapListener != null) {
            cardTapListener.transmissionError();
            resetListener();
        }
    }

    private void resetListener(){
        cardTapListener = null;
    }

    public IsoDep getmIsoDep() {
        return mIsoDep;
    }
}
